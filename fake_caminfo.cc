/****************************************************************************

Subscribe to a Image topic and publish a fake CamInfo with matched times


****************************************************************************/

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>

using namespace std;

// Here I use global publisher and subscriber, since I want to access the
// publisher in the function MsgCallback:
ros::Publisher ci_pub;
image_transport::Subscriber image_subs;


void imageCallback(const sensor_msgs::Image::ConstPtr &msg)
{
  sensor_msgs::CameraInfo ci;
  ci.header = msg->header;
  ci.distortion_model = "plumb_bob";
  ci.height = 480;
  ci.width = 640;
  ci.D = {0.0, 0.0, 0.0, 0.0, 0.0};
  ci.K = {570.3422241210938, 0.0, 319.5, 0.0, 570.3422241210938, 239.5, 0.0, 0.0, 1.0};
  ci.R = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  ci.P = {570.3422241210938, 0.0, 319.5, 0.0, 0.0, 570.3422241210938, 239.5, 0.0, 0.0, 0.0, 1.0, 0.0};
  

  ci_pub.publish(ci);

}

int main(int argc, char **argv)
{
  string image_topic_;
  string caminfo_topic_;
  
  ros::init(argc, argv, "fake_caminfo", ros::init_options::AnonymousName );
  ros::NodeHandle pnh("~");
  image_transport::ImageTransport *it = new image_transport::ImageTransport(pnh);
  image_transport::Subscriber subs;
  
  if(!pnh.getParam("image_topic", image_topic_)){
    image_topic_ = "/image_raw";
  }
  
  if(!pnh.getParam("caminfo_topic", caminfo_topic_)){
    caminfo_topic_ = "/camera_info";
  }
  
  ROS_INFO("Reading images from [%s]", image_topic_.c_str());
  ROS_INFO("Publishing caminfo to [%s]", caminfo_topic_.c_str());
  ci_pub = pnh.advertise<sensor_msgs::CameraInfo>(caminfo_topic_, 1);
  image_subs = it->subscribe(image_topic_, 1000, imageCallback);
  while (ros::ok())
    {
      ros::spinOnce();
    }

  return 0;
}
